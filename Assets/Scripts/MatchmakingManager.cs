using PlayFab;
using PlayFab.MultiplayerModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchmakingManager : MonoBehaviour
{
    public static MatchmakingManager Singleton { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (Singleton != null && Singleton != this)
        {
            Destroy(this);
        }
        else
        {
            Singleton = this;
        }
    }

    public void FindMatch()
    {
        string entityId = AuthenticationManager.Singleton.entityId;
        string entityType = AuthenticationManager.Singleton.entityType;

        CreateMatchmakingTicket(entityId, entityType);
    }

    private void CreateMatchmakingTicket(string entityId, string entityType)
    {
        PlayFabMultiplayerAPI.CreateMatchmakingTicket(
            new CreateMatchmakingTicketRequest
            {
                // The ticket creator specifies their own player attributes.
                Creator = new MatchmakingPlayer
                {
                    Entity = new EntityKey
                    {
                        Id = entityId,
                        Type = entityType,
                    },

                    // Here we specify the creator's attributes.
                    Attributes = new MatchmakingPlayerAttributes
                    {
                        DataObject = new
                        {
                            Latencies = new object[]
                            {
                                new
                                {
                                    region = "NorthEurope",
                                    latency = 150
                                },
                            }
                        },
                    },
                },

                // Cancel matchmaking if a match is not found after 120 seconds.
                GiveUpAfterSeconds = 120,

                // The name of the queue to submit the ticket into.
                QueueName = "RoyaleQueue"
            },
            OnMatchmakingTicketCreated,
            OnFailure
        );
    }

    private void OnMatchmakingTicketCreated(CreateMatchmakingTicketResult result)
    {
        // Poll matchmaking ticket
        GetMatchmakingTicket(result.TicketId);
    }

    private void GetMatchmakingTicket(string ticketId)
    {
        PlayFabMultiplayerAPI.GetMatchmakingTicket(
            new GetMatchmakingTicketRequest
            {
                TicketId = ticketId,
                QueueName = "RoyaleQueue",
            },
            OnGetMatchmakingTicket,
            OnFailure);
    }

    private void OnGetMatchmakingTicket(GetMatchmakingTicketResult result)
    {
        StartCoroutine(StartPolling(result));

        //if (result.Status == "Matched")
        //{
        //    Debug.Log("Match ready");
        //    GetMatch(result.MatchId, result.QueueName);
        //}
        //else
        //{
        //    Debug.Log("Poll start");
        //    StartCoroutine(PollPause());
        //    Debug.Log("Poll end");
        //    GetMatchmakingTicket(result.TicketId);
        //}
    }

    private IEnumerator StartPolling(GetMatchmakingTicketResult result)
    {
        if (result.Status == "Matched")
        {
            Debug.Log("Match ready");
            GetMatch(result.MatchId, result.QueueName);
        }
        else
        {
            Debug.Log("Poll start");
            yield return new WaitForSeconds(6f);
            Debug.Log("Poll end");
            GetMatchmakingTicket(result.TicketId);
        }
    }

    /// <summary>
    /// Temporary polling mechanism until PlayFab supports notifications/subscriptions.
    /// </summary>
    /// <returns>Wait for 6 seconds.</returns>
    private IEnumerator PollPause()
    {
        for (int iCnt = 0; iCnt < 10; iCnt++)
        {
            yield return new WaitForSeconds(6f);
            Debug.Log(string.Format("Timer = {0}", Time.time));
        }
    }

    private void GetMatch(string matchId, string queueName)
    {
        PlayFabMultiplayerAPI.GetMatch(
            new GetMatchRequest
            {
                MatchId = matchId,
                QueueName = queueName,
            },
            OnGetMatch,
            OnFailure);
    }

    private void OnGetMatch(GetMatchResult result)
    {
        MultiplayerManager.Singleton.ConnectToMatchServer(result);
    }

    private void OnFailure(PlayFabError error)
    {
        Debug.Log("[OnFailure] MatchmakingManager Error: " + error);
    }
}
