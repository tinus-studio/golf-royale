using Unity.Netcode;
using UnityEngine;

public class ServerManager : MonoBehaviour
{
    public static ServerManager Singleton { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (Singleton != null && Singleton != this)
        {
            Destroy(this);
        }
        else
        {
            Singleton = this;
        }
    }

    private void Start()
    {
        NetworkManager.Singleton.OnServerStarted += NetworkManager_OnServerStarted;
        NetworkManager.Singleton.OnClientConnectedCallback += NetworkManager_OnClientConnectedCallback;
        NetworkManager.Singleton.OnClientDisconnectCallback += NetworkManager_OnClientDisconnectCallback;
    }

    public void StartServer()
    {
        NetworkManager.Singleton.StartServer();
    }

    public void ShutdownServer()
    {
        NetworkManager.Singleton.Shutdown();
    }

    private void NetworkManager_OnServerStarted()
    {
        Debug.Log("Netcode Server Started");
    }

    private void NetworkManager_OnClientConnectedCallback(ulong obj)
    {
        Debug.Log("Client Connected");
        // ClientRPC om iets te laten weten aan client vanuit de server?
        // + update connected players count in PlayFab
    }

    private void NetworkManager_OnClientDisconnectCallback(ulong obj)
    {
        Debug.Log("Client Disconnected");
        // ClientRPC om iets te laten weten aan client vanuit de server?
        // + update connected players count in PlayFab
    }
}
