using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.Events;

public class S_NetcodeServer : MonoBehaviour
{
    public class PlayerEvent : UnityEvent<string> { }
    public PlayerEvent OnPlayerAdded = new();
    public PlayerEvent OnPlayerRemoved = new();

    public void StartServer()
    {
        NetworkManager.Singleton.StartServer();
    }

    public void ShutdownServer()
    {
        NetworkManager.Singleton.Shutdown();
    }

    private void Awake()
    {
        NetworkManager.Singleton.OnServerStarted += NetworkManager_OnServerStarted;
        NetworkManager.Singleton.OnClientConnectedCallback += NetworkManager_OnClientConnectedCallback;
        NetworkManager.Singleton.OnClientDisconnectCallback += NetworkManager_OnClientDisconnectCallback;
    }

    private void NetworkManager_OnServerStarted()
    {
        Debug.Log("Netcode Server Started");
    }

    private void NetworkManager_OnClientConnectedCallback(ulong obj)
    {
        Debug.Log("Client Connected");
        // ClientRPC om iets te laten weten aan client vanuit de server?
        // + update connected players count in PlayFab
    }

    private void NetworkManager_OnClientDisconnectCallback(ulong obj)
    {
        Debug.Log("Client Disconnected");
        // ClientRPC om iets te laten weten aan client vanuit de server?
        // + update connected players count in PlayFab
    }
}
