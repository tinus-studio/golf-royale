using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using System;
using PlayFab.MultiplayerAgent.Model;
using Unity.Netcode;

public class S_StartPlayFabAgent : MonoBehaviour
{
    public X_Statics statics;
    private List<ConnectedPlayer> _connectedPlayers;

    public S_NetcodeServer netcodeServer;

    public void StartPlayFabAgent()
    {
        _connectedPlayers = new List<ConnectedPlayer>();

        PlayFabMultiplayerAgentAPI.Start();

        //NetcodeServer.OnPlayerAdded.AddListener(OnPlayerAdded);
        //NetcodeServer.OnPlayerRemoved.AddListener(OnPlayerRemoved);

        StartCoroutine(ReadyForPlayers());
    }

    private void Awake()
    {
        PlayFabMultiplayerAgentAPI.IsDebugging = statics.playfabDebugging;
        PlayFabMultiplayerAgentAPI.OnServerActiveCallback += PlayFabMultiplayerAgentAPI_OnServerActiveCallback;
        PlayFabMultiplayerAgentAPI.OnShutDownCallback += PlayFabMultiplayerAgentAPI_OnShutDownCallback;
        PlayFabMultiplayerAgentAPI.OnMaintenanceCallback += PlayFabMultiplayerAgentAPI_OnMaintenanceCallback;
        PlayFabMultiplayerAgentAPI.OnAgentErrorCallback += PlayFabMultiplayerAgentAPI_OnAgentErrorCallback;
    }

    private IEnumerator ReadyForPlayers()
    {
        yield return new WaitForSeconds(.5f);
        PlayFabMultiplayerAgentAPI.ReadyForPlayers();
    }

    private void PlayFabMultiplayerAgentAPI_OnServerActiveCallback()
    {
        netcodeServer.StartServer();
        Debug.Log("Server Started From Agent Activation");
    }

    private void PlayFabMultiplayerAgentAPI_OnShutDownCallback()
    {
        Debug.Log("Server is Shutting down");
        netcodeServer.ShutdownServer();

        StartCoroutine(Shutdown());
    }

    private IEnumerator Shutdown()
    {
        yield return new WaitForSeconds(5f);
        Application.Quit();
    }

    private void PlayFabMultiplayerAgentAPI_OnMaintenanceCallback(DateTime? NextScheduledMaintenanceUtc)
    {
        throw new NotImplementedException();
    }

    private void PlayFabMultiplayerAgentAPI_OnAgentErrorCallback(string error)
    {
        Debug.Log(error);
    }
}
