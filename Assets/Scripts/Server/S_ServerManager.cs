using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_ServerManager : MonoBehaviour
{
    public S_StartPlayFabAgent startPlayFabAgent;

    public void Initialize()
    {
        startPlayFabAgent.StartPlayFabAgent();
    }
}
