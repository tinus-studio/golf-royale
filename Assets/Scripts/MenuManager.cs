using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public int shotCountLabel = 0;

    public static MenuManager Singleton { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (Singleton != null && Singleton != this)
        {
            Destroy(this);
        }
        else
        {
            Singleton = this;
        }
    }

    private void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, 300, 300));

        if (GUILayout.Button("Login")) AuthenticationManager.Singleton.Login();
        if (GUILayout.Button("Find Match")) MatchmakingManager.Singleton.FindMatch();
        if (GUILayout.Button("Start Client")) NetworkManager.Singleton.StartClient();
        if (GUILayout.Button("Change Scene")) NetworkManager.Singleton.SceneManager.LoadScene("MatchScene", LoadSceneMode.Single);

        GUILayout.EndArea();
    }
}
