using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Singleton { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (Singleton != null && Singleton != this)
        {
            Destroy(this);
        }
        else
        {
            Singleton = this;
        }
    }

    private void Start()
    {
        if (Application.isEditor)
        {
            //START CLIENT
            //clientManager.Initialize();
        }
        else
        {
            var args = GetCommandlineArgs();

            if (args.TryGetValue("-netcode", out string netcodeValue))
            {
                switch (netcodeValue)
                {
                    case "server":
                        //START NETCODE SERVER (on PlayFab VM)
                        MultiplayerManager.Singleton.StartPlayFabAgent();
                        break;
                    case "client":
                        //START CLIENT (on local mobile)
                        //clientManager.Initialize();
                        break;
                }
            }
        }
    }

    private Dictionary<string, string> GetCommandlineArgs()
    {
        Dictionary<string, string> argDictionary = new Dictionary<string, string>();

        var args = System.Environment.GetCommandLineArgs();

        for (int i = 0; i < args.Length; ++i)
        {
            var arg = args[i].ToLower();
            if (arg.StartsWith("-"))
            {
                var value = i < args.Length - 1 ? args[i + 1].ToLower() : null;
                value = (value?.StartsWith("-") ?? false) ? null : value;

                argDictionary.Add(arg, value);
            }
        }
        return argDictionary;
    }
}
