using PlayFab;
using PlayFab.MultiplayerModels;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.Netcode.Transports.UNET;
using UnityEngine;

public class MultiplayerManager : MonoBehaviour
{
    private List<ConnectedPlayer> _connectedPlayers;
    public static MultiplayerManager Singleton { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (Singleton != null && Singleton != this)
        {
            Destroy(this);
        }
        else
        {
            Singleton = this;
        }
    }

    public void ConnectToMatchServer(GetMatchResult match)
    {
        NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectAddress = match.ServerDetails.IPV4Address;
        NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectPort = (ushort)match.ServerDetails.Ports[0].Num;

        // Load Scene
        NetworkManager.Singleton.SceneManager.LoadScene("MatchScene", UnityEngine.SceneManagement.LoadSceneMode.Single);

        // Connect Client
        NetworkManager.Singleton.StartClient();
        Debug.Log("[ConnectToMatchServer] Client started!!");
    }

    public void StartPlayFabAgent()
    {
        _connectedPlayers = new List<ConnectedPlayer>();

        PlayFabMultiplayerAgentAPI.Start();

        //NetcodeServer.OnPlayerAdded.AddListener(OnPlayerAdded);
        //NetcodeServer.OnPlayerRemoved.AddListener(OnPlayerRemoved);

        StartCoroutine(ReadyForPlayers());
    }

    private void Start()
    {
        PlayFabMultiplayerAgentAPI.IsDebugging = true;
        PlayFabMultiplayerAgentAPI.OnServerActiveCallback += PlayFabMultiplayerAgentAPI_OnServerActiveCallback;
        PlayFabMultiplayerAgentAPI.OnShutDownCallback += PlayFabMultiplayerAgentAPI_OnShutDownCallback;
        PlayFabMultiplayerAgentAPI.OnMaintenanceCallback += PlayFabMultiplayerAgentAPI_OnMaintenanceCallback;
        PlayFabMultiplayerAgentAPI.OnAgentErrorCallback += PlayFabMultiplayerAgentAPI_OnAgentErrorCallback;
    }

    private IEnumerator ReadyForPlayers()
    {
        yield return new WaitForSeconds(.5f);
        PlayFabMultiplayerAgentAPI.ReadyForPlayers();
    }

    private void PlayFabMultiplayerAgentAPI_OnServerActiveCallback()
    {
        ServerManager.Singleton.StartServer();
        Debug.Log("Server Started From Agent Activation");
    }

    private void PlayFabMultiplayerAgentAPI_OnShutDownCallback()
    {
        Debug.Log("Server is Shutting down");
        ServerManager.Singleton.ShutdownServer();

        StartCoroutine(Shutdown());
    }

    private IEnumerator Shutdown()
    {
        yield return new WaitForSeconds(5f);
        Application.Quit();
    }

    private void PlayFabMultiplayerAgentAPI_OnMaintenanceCallback(DateTime? NextScheduledMaintenanceUtc)
    {
        throw new NotImplementedException();
    }

    private void PlayFabMultiplayerAgentAPI_OnAgentErrorCallback(string error)
    {
        Debug.Log(error);
    }
}
