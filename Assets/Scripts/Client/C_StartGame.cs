using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using System;
using PlayFab.MultiplayerModels;
using Unity.Netcode;
using Unity.Netcode.Transports.UNET;

public class C_StartGame : MonoBehaviour
{
    public X_Statics statics;
    public C_Login login;

    private string buildId;
    private string serverId;
    private string sessionId;

    public void StartGame()
    {
        // Search for and try to join available game
        if (SearchGame())
        {
            //PlayFabMultiplayerAPI.GetBuild

            // MOVE TO SERVER | IMPLEMENT MULTIPLAYER MATCHMAKING
            ListBuildSummariesRequest listBuildSummariesRequest = new();
            listBuildSummariesRequest.AuthenticationContext = login.playFabAuthenticationContext;
            PlayFabMultiplayerAPI.ListBuildSummariesV2(listBuildSummariesRequest, OnListBuildSummaries, OnFailure);

            // MOVE TO SERVER | IMPLEMENT MULTIPLAYER MATCHMAKING
            ListMultiplayerServersRequest listMultiplayerServersRequest = new(); // recommend that you call ListMultiplayerServers and return the result back to the client from the server
            listMultiplayerServersRequest.AuthenticationContext = login.playFabAuthenticationContext;
            listMultiplayerServersRequest.BuildId = buildId;
            listMultiplayerServersRequest.Region = AzureRegion.NorthEurope.ToString();
            PlayFabMultiplayerAPI.ListMultiplayerServers(listMultiplayerServersRequest, OnListMultiplayerServers, OnFailure);

            RequestMultiplayerServerRequest requestMultiplayerServerRequest = new();
            requestMultiplayerServerRequest.BuildId = buildId;
            requestMultiplayerServerRequest.SessionId = sessionId;
            requestMultiplayerServerRequest.PreferredRegions = new List<string>() { AzureRegion.NorthEurope.ToString() };
            PlayFabMultiplayerAPI.RequestMultiplayerServer(requestMultiplayerServerRequest, OnRequestMultiplayerServer, OnFailure);


            //PlayFabMultiplayerAPI.CreateMatchmakingTicket

            //PlayFabMultiplayerAPI.GetMultiplayerServerDetails

            //JoinMatchmakingTicketRequest joinMatchmakingTicketRequest = new();
            //joinMatchmakingTicketRequest.
            //PlayFabMultiplayerAPI.JoinMatchmakingTicket


            Debug.Log("Game Found");
        }
        else // If no game is found, request new game (playfab VM)
        {
            RequestGame();
        }

    }

    private void OnListBuildSummaries(ListBuildSummariesResponse listBuildSummariesResponse)
    {
        List<BuildSummary> buildSummaries = listBuildSummariesResponse.BuildSummaries;
        buildId = buildSummaries[0].BuildId;
        Debug.Log("BuildId: " + buildId);
    }

    private void OnListMultiplayerServers(ListMultiplayerServersResponse listMultiplayerServersResponse)
    {
        List<MultiplayerServerSummary> multiplayerServerSummaries = listMultiplayerServersResponse.MultiplayerServerSummaries;
        serverId = multiplayerServerSummaries[0].ServerId;
        Debug.Log("ServerId: " + serverId);
        sessionId = multiplayerServerSummaries[0].SessionId;
        Debug.Log("SessionId: " + sessionId);
    }

    private bool SearchGame()
    {
        return true;
    }

    private void RequestGame()
    {
        RequestMultiplayerServerRequest requestMultiplayerServerRequest = new();
        requestMultiplayerServerRequest.BuildId = statics.buildId;
        requestMultiplayerServerRequest.SessionId = Guid.NewGuid().ToString();
        requestMultiplayerServerRequest.PreferredRegions = new List<string>() { AzureRegion.NorthEurope.ToString() };
        PlayFabMultiplayerAPI.RequestMultiplayerServer(requestMultiplayerServerRequest, OnRequestMultiplayerServer, OnFailure);
    }

    private void OnRequestMultiplayerServer(RequestMultiplayerServerResponse requestMultiplayerServerResponse)
    {
        // Set IPV4Address and Port
        NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectAddress = requestMultiplayerServerResponse.IPV4Address;
        NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectPort = requestMultiplayerServerResponse.Ports[0].Num;

        Debug.Log("New multiplayer server requested! | PlayFab VM started?!");
        Debug.Log("IP: " + requestMultiplayerServerResponse.IPV4Address + " Port: " + (ushort)requestMultiplayerServerResponse.Ports[0].Num);

        // Connect to netcode server running in this VM. PERHAPS THIS CALL IS TO EARLY / AKA NETCODE SERVER IS NOT STARTED YET.
        NetworkManager.Singleton.StartClient();
    }

    private void OnFailure(PlayFabError error)
    {
        Debug.Log(error);
    }
}
