using Unity.Netcode;
using UnityEngine;

public class C_Player : MonoBehaviour
{
    // Serialize fields
    [SerializeField] private float shotPower, maxForce, minSpeed;
    // Render line (trail)
    [SerializeField] private LineRenderer lineRenderer;

    private Rigidbody rigidbody;
    private float shotForce;

    // startPos = mouseclick
    // endPos = release mouse
    // direction = angle
    private Vector3 startPos, endPos, direction;

    // Bool value default = false
    private bool canShoot, shotStarted;

    private void Start()
    {
        // Get rigidbody component of this object.
        rigidbody = GetComponent<Rigidbody>();

        canShoot = true;

        // When to certain value (minSpeed) set rigidbody to sleep.
        rigidbody.sleepThreshold = minSpeed;
    }

    private void OnMouseDown()
    {
        startPos = MousePositionInWorld();

        shotStarted = true;

        // Make only visible when taking a shot.
        lineRenderer.gameObject.SetActive(true);
        lineRenderer.SetPosition(0, lineRenderer.transform.localPosition);
    }

    private void OnMouseDrag()
    {
        endPos = MousePositionInWorld();

        // Set (clamp) the shot force
        shotForce = Mathf.Clamp(Vector3.Distance(endPos, startPos), 0, maxForce);

        // Draw line renderer
        lineRenderer.SetPosition(1, transform.InverseTransformPoint(endPos));
    }

    private void OnMouseUp()
    {
        canShoot = false;
        shotStarted = false;

        // Make line invisble when ball is released.
        lineRenderer.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (!canShoot)
        {
            // Set the angle
            direction = startPos - endPos;

            // ForceMode Impulse is an instant force.
            rigidbody.AddForce(Vector3.Normalize(direction) * shotForce * shotPower, ForceMode.Impulse);

            // Reset postitions
            startPos = endPos = Vector3.zero;
        }

        if (rigidbody.IsSleeping())
        {
            canShoot = true;
        }
    }

    private Vector3 MousePositionInWorld()
    {
        // Can be seen as "empty" vector.
        // Where x,y,z are all 0.
        Vector3 position = Vector3.zero;

        // Creates a 'ray'.
        // A line from the main camera to the mouse postion. 
        // Screen point = mouse position.
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Get information out of a raycast.
        RaycastHit hit = new RaycastHit();

        // Use the physics engine to generate a raycast.
        if (Physics.Raycast(ray, out hit))
        {
            position = hit.point;
        }

        return position;
    }

    [ServerRpc]
    private void Shoot()
    {
        // EMPTY
    }
}
