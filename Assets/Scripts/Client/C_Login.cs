using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

public class C_Login : MonoBehaviour
{
    public PlayFabAuthenticationContext playFabAuthenticationContext;

    public void Login()
    {
        var loginRequest = new LoginWithAndroidDeviceIDRequest
        {
            AndroidDeviceId = SystemInfo.deviceUniqueIdentifier.ToString(),
            CreateAccount = true
        };

        PlayFabClientAPI.LoginWithAndroidDeviceID(loginRequest, OnLoginSucces, OnFailure);
    }

    private void OnLoginSucces(LoginResult loginResult)
    {
        if (loginResult.NewlyCreated)
        {
            // Set a new display name for user when newly created.
            PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest
            {
                DisplayName = "Guest_" + loginResult.PlayFabId
            },
            loginResult => {
                Debug.Log("Updated display name: " + loginResult.DisplayName);
            }, 
            OnFailure);

            // Log successful register
            Debug.Log("Successful register (PlayFabId): " + loginResult.PlayFabId);
        }

        // Log successful login
        Debug.Log("Successful login (PlayFabId): " + loginResult.PlayFabId);

        // Set Auth Context
        playFabAuthenticationContext = loginResult.AuthenticationContext;
    }

    private void OnFailure(PlayFabError error)
    {
        throw new NotImplementedException();
    }
}
