using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_ClientManager : MonoBehaviour
{
    public C_Login clientLogin;
    public C_StartGame startGame;

    // Initialize client
    public void Initialize()
    {
        // Login or Register PlayFab user.
        clientLogin.Login();
    }

    public void StartGame()
    {
        // Try to find or start a new game
        startGame.StartGame();
    }
}
