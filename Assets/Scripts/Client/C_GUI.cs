using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_GUI : MonoBehaviour
{
    public C_ClientManager clientManager;

    private void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, 300, 300));

        if (GUILayout.Button("Start Game")) clientManager.StartGame();

        GUILayout.EndArea();
    }
}
