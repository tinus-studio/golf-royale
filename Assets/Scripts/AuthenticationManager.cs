using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

public class AuthenticationManager : MonoBehaviour
{
    public string entityId;
    public string entityType;

    public static AuthenticationManager Singleton { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (Singleton != null && Singleton != this)
        {
            Destroy(this);
        }
        else
        {
            Singleton = this;
        }
    }

    public void Login()
    {
        var loginRequest = new LoginWithAndroidDeviceIDRequest
        {
            AndroidDeviceId = SystemInfo.deviceUniqueIdentifier.ToString(),
            //AndroidDeviceId = "123",
            CreateAccount = true
        };

        PlayFabClientAPI.LoginWithAndroidDeviceID(loginRequest, OnLoginSucces, OnFailure);
    }

    private void OnLoginSucces(LoginResult loginResult)
    {
        if (loginResult.NewlyCreated)
        {
            // Set a new display name for user when newly created.
            PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest
            {
                DisplayName = "Guest_" + loginResult.PlayFabId
            },
            loginResult => {
                Debug.Log("Updated display name: " + loginResult.DisplayName);
            },
            OnFailure);

            // Log successful register
            Debug.Log("Successful register (PlayFabId): " + loginResult.PlayFabId);
        }

        // Log successful login
        Debug.Log("Successful login (PlayFabId): " + loginResult.PlayFabId);

        // Set Entity
        entityId = loginResult.EntityToken.Entity.Id;
        entityType = loginResult.EntityToken.Entity.Type;

        // Set Auth Context
        //playFabAuthenticationContext = loginResult.AuthenticationContext;
    }

    private void OnFailure(PlayFabError error)
    {
        throw new NotImplementedException();
    }
}
