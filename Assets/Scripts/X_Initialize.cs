using System.Collections.Generic;
using UnityEngine;

public class X_Initialize : MonoBehaviour
{
    public C_ClientManager clientManager;
    public S_ServerManager serverManager;

    // Start is called before the first frame update
    void Start()
    {
        if (Application.isEditor)
        {
            //START CLIENT
            clientManager.Initialize();
        }
        else
        {
            var args = GetCommandlineArgs();

            if (args.TryGetValue("-netcode", out string netcodeValue))
            {
                switch (netcodeValue)
                {
                    case "server":
                        //START NETCODE SERVER (on PlayFab VM)
                        serverManager.Initialize();
                        break;
                    case "client":
                        //START CLIENT (on local mobile)
                        clientManager.Initialize();
                        break;
                }
            }
        }

    }

    private Dictionary<string, string> GetCommandlineArgs()
    {
        Dictionary<string, string> argDictionary = new Dictionary<string, string>();

        var args = System.Environment.GetCommandLineArgs();

        for (int i = 0; i < args.Length; ++i)
        {
            var arg = args[i].ToLower();
            if (arg.StartsWith("-"))
            {
                var value = i < args.Length - 1 ? args[i + 1].ToLower() : null;
                value = (value?.StartsWith("-") ?? false) ? null : value;

                argDictionary.Add(arg, value);
            }
        }
        return argDictionary;
    }
}
