using System;
using Unity.Netcode;
using UnityEngine;

[RequireComponent(typeof(NetworkObject))]
public class Player : NetworkBehaviour
{
    // Serialize fields
    [SerializeField] private float shotPower, maxForce, minSpeed;
    // Render line (trail)
    [SerializeField] private LineRenderer lineRenderer;

    private Rigidbody rb;
    private float shotForce;
    public float thrust = 20f;

    private NetworkVariable<int> shotCount = new(0);

    // startPos = mouseclick
    // endPos = release mouse
    // direction = angle
    private Vector3 startPos, endPos, direction;

    // Bool value default = false
    private bool canShoot, shotStarted;

    private void Start()
    {
        // Get rigidbody component of this object.
        rb = GetComponent<Rigidbody>();

        canShoot = true;

        // When to certain value (minSpeed) set rigidbody to sleep.
        rb.sleepThreshold = minSpeed;

        // Show shoutCount in logs.
        shotCount.OnValueChanged += (int prevI, int newI) =>
        {
            Debug.Log("shotCount went from " + prevI + " to " + newI);
        };
    }

    private void OnGUI() // TODO move this to MatchManager or MatchUIManager
    {
        GUILayout.BeginArea(new Rect(10, 10, 300, 300));

        GUILayout.Label("Shotcount: " + shotCount.Value.ToString());

        GUILayout.EndArea();
    }

    private void OnMouseDown()
    {
        startPos = MousePositionInWorld();

        shotStarted = true;

        // Make only visible when taking a shot.
        lineRenderer.gameObject.SetActive(true);
        lineRenderer.SetPosition(0, lineRenderer.transform.localPosition);
    }

    private void OnMouseDrag()
    {
        endPos = MousePositionInWorld();

        // Set (clamp) the shot force
        shotForce = Mathf.Clamp(Vector3.Distance(endPos, startPos), 0, maxForce);

        // Draw line renderer
        lineRenderer.SetPosition(1, transform.InverseTransformPoint(endPos));
    }

    private void OnMouseUp()
    {
        canShoot = false;
        shotStarted = false;

        // Make line invisble when ball is released.
        lineRenderer.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        //ShootServerRpc();

        if (Input.GetButton("Jump"))
        {
            JumpServerRpc();
        }
    }

    private Vector3 MousePositionInWorld()
    {
        // Can be seen as "empty" vector.
        // Where x,y,z are all 0.
        Vector3 position = Vector3.zero;

        // Creates a 'ray'.
        // A line from the main camera to the mouse postion. 
        // Screen point = mouse position.
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Get information out of a raycast.
        RaycastHit hit = new();

        // Use the physics engine to generate a raycast.
        if (Physics.Raycast(ray, out hit))
        {
            position = hit.point;
        }

        return position;
    }

    [ServerRpc(RequireOwnership = false)]
    private void JumpServerRpc()
    {
        rb.AddForce(transform.forward * thrust);
        shotCount.Value++;
    }

    [ServerRpc(RequireOwnership = false)]
    private void ShootServerRpc(ServerRpcParams rpcParams = default)
    {
        if (!canShoot)
        {
            // Set the angle
            direction = startPos - endPos;

            // ForceMode Impulse is an instant force.
            GetComponent<Rigidbody>().AddForce(Vector3.Normalize(direction) * shotForce * shotPower, ForceMode.Impulse);

            // Reset postitions
            startPos = endPos = Vector3.zero;
        }

        if (GetComponent<Rigidbody>().IsSleeping())
        {
            canShoot = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Hole") //TODO replace with 'cleaner' solution.
        {
            // Send ball in hole trigger to server.
            BallInHoleTrigger();
        }
    }

    private void BallInHoleTrigger()
    {
        
    }
}
